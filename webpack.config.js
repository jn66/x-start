// 引入node中的路径模块
const path =  require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

// 导出配置内容
module.exports = {
    mode:'development',
    // 入口文件
    entry:{
        index:"./src/index.js",
        app:"./src/app.js"
    },
    //追踪错误
    // devtool: 'inline-source-map', 
    // 配置webpack-dev-server
    devServer:{
        static:'./dist'
    },
    //出口文件
    output:{
        filename: 'js/[name].bundle.js',
        path: path.resolve(__dirname, 'dist'),
        clean: true,  //以前要用CleanWebpackPlugin 插件，现在不用了
    },
    //多文件入口要用这个
    optimization:{
        runtimeChunk:'single',
    },
    //解决文件过大报错
    performance:{
        maxAssetSize: 3000000,
        maxEntrypointSize: 5000000
    },
    module:{
        rules:[
            {
                test:/\.css$/,
                use: [
                    //把转到js的css片段 插入页面中<style> 标签内
                    'style-loader',
                    //把css文件转到js的文件内，新版css-loader能自动处理图片,
                    //自动生成新的图片，重命名，并打包后修改css地址
                    'css-loader'
                ]
            },
            {
                test:/\.(png|svg|jpg|gif)$/,
                //之前是用file-loader解决，现在声明类型后，webpack自己解决。
                type: 'asset/resource',
                generator: {
                    filename: 'img/[name]_[hash:8][ext]'
                }
            },
            {
                //处理html中 image图片的
                test:/\.html$/,
                use:'html-loader',
            },
            {
                exclude:/\.(html|js|css|jpg|png|gif)$/,
                type: 'asset/resource',
                generator: {
                    filename: 'media/[name]-[hash:8].[ext]'
                }
            }
        ]
    },
    plugins: [
        // 生成一个index.html文件，并且把刚才生成的两个js用script的方式写进去
        new HtmlWebpackPlugin({
            title: 'output',
            template:'./src/index.html'
        })
    ]
}