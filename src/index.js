import _ from 'lodash';
import './style.css';
import './style2.css';
import Icon from './img.png';

function component() {
    var element = document.createElement('div');
  
    // Lodash（目前通过一个 script 脚本引入）对于执行这一行是必需的  Lodash, now imported by this script
    // element.innerHTML = _.join(['Hello', 'webpack'], ' ');
    element.innerHTML ='a1bc';
    element.classList.add('hello');

    var myIcon = new Image();
    myIcon.src = Icon;

    element.appendChild(myIcon);
  
    return element;
  }
  
  console.log(_.join(['Another1', 'module2', 'loaded!3'], ' '));
  document.body.appendChild(component());