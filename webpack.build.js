const path =  require('path');
const HtmlWebpackPlugin = require("html-webpack-plugin");
//提取css成单独文件，并且插入到link标签
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
// 压缩css
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
// 设置node的环境变量为开发环境，不设置就是默认生产环境。告诉postcss是开发环境
process.env.NODE_ENV = "development";

module.exports = {
    entry: './src/index.js',
    output: {
        filename: 'js/built.js',
        path: path.resolve(__dirname, 'dist'),
        clean: true,
    },
    module:{
        rules:[
            {
                test: /\.css$/,
                use: [
                    //取代style-loader，作用，提取js中的css成单独文件
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    //使用loader的默认配置 ‘postcss-loader’
                    // 修改loader的配置
                    //postcss插件 帮助postcss找到package中的browserslist的配置
                    // 默认找的是生产环境的browserslist的配置，开发环境得设置环境变量
                    // process.env.NODE_ENV = development
                    //通过配置加载指定兼容性的样式
                    {
                        loader: 'postcss-loader',
                        options:{
                            postcssOptions:{
                                plugins: [
                                    [
                                        "postcss-preset-env"
                                    ]
                                ]

                            }
                        }
                    }
                ]
            },
            {
                test:/\.(png|svg|jpg|gif)$/,
                //之前是用file-loader解决，现在声明类型后，webpack自己解决。
                type: 'asset/resource',
                generator: {
                    filename: 'img/[name]_[hash:8][ext]'
                }
            },
        ]
    },
    optimization: {
        minimizer: [
          // For webpack@5 you can use the `...` syntax to extend existing minimizers (i.e. `terser-webpack-plugin`), uncomment the next line
          // `...`,压缩css
          new CssMinimizerPlugin(),
        ],
    },
    plugins:[
        new HtmlWebpackPlugin({
            title: 'output',
            template:'./src/index.html'
        }),
        new MiniCssExtractPlugin({
            //对输出的css进行重命名
            filename:'css/built.css'
        })
    ],
    mode: 'production'
}