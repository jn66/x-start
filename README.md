# x-start

一个webpack脚手架

在前端开发中，有各种各样的奇怪的需求。

1.有的只要静态页面，交给后端进行模板套用。

2.有的要前后分离，但是不用vue，就用jQuery，简单粗暴。需要生成生产模式压缩过的一些代码。

3.有的要用vue、react，要做单页面应用，所以直接用现成的脚手架。

这个脚手架是干嘛用的？

就是主要针对前两点，要静态页的，引入一些sass转换呀，html的头部包含等内容。减少相同内容的频繁修改。

要前后分离的，进行一些打包。

src文件夹下是源码，dist下是生成的打包后的文件。

# 小教程
```bash
npm init -y //初始化npm包
npm install webpack webpack-cli --save-dev  //在本包内安装 webpack 和webpack-cli
pnpm add -D style-loader css-loader  //用pnpm包安装好多loader


```

用power shell安装 pnpm
```bash 
$env:PNPM_VERSION='7.0.0-rc.2' ; iwr https://get.pnpm.io/install.ps1 -useb | iex
```

# 开始使用
```
npm install 或 pnpm install
npm run dev 开发模式
npm run build 打包编译一次
```


# 版本历史
* 2022-5-11
拆分开生产模式和开发模式，在生产模式中，使用html-webpack-plugin插件，创建css文件，自动在html文件中用link标签引入样式。
而不是之前的在js中引入，这样就不会在加载js的时候出现闪屏。
使用css-minimizer-webpack-plugin插件压缩生产模式的输出的css

* 2022-5-10
打包后，js、图片，分别进入不同的目录

* 2022-5-9 
使用html-webpack-plugin，自动生成index.html文件，并自动引入js.
完成原CleanWebpackPlugin的插件每次打包清除文件的功能，现在webpack内置，仅需简单配置。
解决文件过大报错，增加performance。
实现webpack server功能，每次修改，浏览器实时刷新。
增加source-map，追踪错误

* 2022-5-8 
使用pnpm包来管理, 速度更快！

* 2022-5-7 
初步上线，在努力中！！

# 待办
css中的图片使用相对路径
小图片转换base64

# 待待办
使用eslint elint airbnb